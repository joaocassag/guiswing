/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author joaocassamano
 */
public class LayeredPayneGui {
    
    public static void main(String[] args){
        JLabel label1 = new JLabel();
        label1.setOpaque(true);
        label1.setBackground(Color.blue);
        label1.setBounds(100,100,200,200);
        
        JLabel label2 = new JLabel();
        label2.setOpaque(true);
        label2.setBackground(Color.green);
        label2.setBounds(150,150,200,200);
        
        JLabel label3 = new JLabel();
        label3.setOpaque(true);
        label3.setBackground(Color.red);
        label3.setBounds(0,0,200,200);
        
        
        JLayeredPane pane = new JLayeredPane();
        pane.setBounds(0,0,500,500);
        
        pane.add(label1, Integer.valueOf(0));
        pane.add(label2,Integer.valueOf(2));
        pane.add(label3,Integer.valueOf(1));
        
        JFrame frame = new JFrame();
        frame.add(pane);
        frame.setTitle("JFrame sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(500,500));
        frame.setLayout(null);
        
        frame.setVisible(true);
    }
        
    
}
