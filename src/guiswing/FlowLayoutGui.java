/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author joaocassamano
 */
public class FlowLayoutGui {
    
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setTitle("JFrame sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setLayout(new FlowLayout(FlowLayout.LEADING));
        frame.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        frame.setSize(500,500);
        
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(190,250));
        panel.setBackground(Color.blue);
        panel.setLayout(new FlowLayout());
        
        panel.add(new JButton("1"));
        panel.add(new JButton("2"));
        panel.add(new JButton("3"));
        panel.add(new JButton("4"));
        panel.add(new JButton("5"));
        panel.add(new JButton("6"));
        panel.add(new JButton("7"));
        panel.add(new JButton("8"));
        panel.add(new JButton("9"));
        
        frame.add(panel);
        frame.setVisible(true);
    }
    
}
