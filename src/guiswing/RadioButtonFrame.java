
package guiswing;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author joaocassamano
 */
public class RadioButtonFrame extends JFrame implements ActionListener {
    JRadioButton one;
    JRadioButton two;
    JRadioButton three;
    
    ImageIcon oneIcon;
    ImageIcon twoIcon;
    ImageIcon threeIcon;
        
    RadioButtonFrame(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        
        //oneIcon = new ImageIcon("src/resources/one.png");
        //twoIcon = new ImageIcon("src/resources/two.png");
        //threeIcon = new ImageIcon("src/resources/three.png");
        
        one = new JRadioButton("1");
        two = new JRadioButton("2");
        three = new JRadioButton("3");
        
        one.setFont(new Font("Noteworthy",Font.BOLD,35));
        two.setFont(new Font("Noteworthy",Font.BOLD,35));
        three.setFont(new Font("Noteworthy",Font.BOLD,35));
        
        //one.setIcon(oneIcon);
        //two.setIcon(twoIcon);
        //three.setIcon(threeIcon);
        
        ButtonGroup group = new ButtonGroup();
        group.add(one);
        group.add(two);
        group.add(three);
        
        one.addActionListener(this);
        two.addActionListener(this);
        three.addActionListener(this);
    
        this.add(one);
        this.add(two);
        this.add(three);
        
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("You clicked option "+((JRadioButton)e.getSource()).getText());
        
    }
}
