/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author joaocassamano
 */
public class CheckBoxFrame extends JFrame implements ActionListener {
    JButton button;
    JCheckBox checkBox;
    ImageIcon yesIcon;
    ImageIcon noIcon;
    CheckBoxFrame(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        
        yesIcon = new ImageIcon("src/resources/yes.png");
        noIcon = new ImageIcon("src/resources/no.png");
        
        button = new JButton("Submit");
        button.addActionListener(this);
        
        checkBox = new JCheckBox();
        checkBox.setText("Sample Text");
        checkBox.setFocusable(false);
        checkBox.setFont(new Font("Times", Font.PLAIN,35));
        checkBox.setIcon(noIcon);
        checkBox.setSelectedIcon(yesIcon);
        
        
        
        this.add(button);
        this.add(checkBox);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button){
            System.out.println("Welcome "+checkBox.isSelected());
        }
    }
        
    
}
