/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;

/**
 *
 * @author joaocassamano
 */
public class Animation2DFrame extends JFrame {
    
    Animation2DPanel panel;
    
    Animation2DFrame(){
        panel = new Animation2DPanel();
        
        this.setTitle("Animation");
        //this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.add(panel);
        this.pack();
        this.setLocationRelativeTo(null);
        
        this.setVisible(true);
    }
    
}
