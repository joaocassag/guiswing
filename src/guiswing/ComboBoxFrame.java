
package guiswing;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author joaocassamano
 */
public class ComboBoxFrame extends JFrame implements ActionListener {
    
    JComboBox comboBox;
    
    ComboBoxFrame(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        
        String[] strNumbers={"1","2","3"};
        
        comboBox = new JComboBox(strNumbers);
        comboBox.addActionListener(this);
        //comboBox.setEditable(true);
        //comboBox.addItem("4");
        //comboBox.insertItemAt("5", 0);
        //comboBox.removeItem("1");
        //comboBox.removeItemAt(0);
        
        this.add(comboBox);
        
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //if(e.getSource()==comboBox){
            System.out.println("Your selected index is "+((JComboBox)e.getSource()).getSelectedIndex());
            System.out.println("Your selected item is "+((JComboBox)e.getSource()).getSelectedItem());
        //}
    }
    
}
