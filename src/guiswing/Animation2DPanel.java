
package guiswing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author joaocassamano
 */
public class Animation2DPanel extends JPanel implements ActionListener {
    final int WIDTH = 500;
    final int HEIGHT = 500;
    Image image;
    Timer timer;
    int xV = 1;
    int yV = 1;
    int x = 0;
    int y = 0;
    Animation2DPanel(){
        image = new ImageIcon("src/resources/save.png").getImage();
        this.setPreferredSize(new Dimension(WIDTH,HEIGHT));
        timer = new Timer(10, this);
        timer.start();
    }
    
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2D = (Graphics2D) g;
        g2D.drawImage(image, x, y,null);
                        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(x>=WIDTH-image.getWidth(null) || x<0){
            xV *=-1;
        }
        
        x = x + xV;
        
        if(y>=HEIGHT-image.getHeight(null)|| y<0){
            yV *=-1;
        }
        
        y = y + yV;
        repaint();
    }
    
}
