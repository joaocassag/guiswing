
package guiswing;

import java.awt.Color;
import java.awt.FlowLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author joaocassamano
 */
public class ProgressBarFeatures {
    
    JFrame frame;
    JProgressBar bar;
    
    ProgressBarFeatures(){
        frame = new JFrame();
        bar = new JProgressBar();
        
        bar.setValue(0);
        bar.setBounds(0, 0, 450,10);
        //bar.setBackground(Color.blue);
        //bar.setForeground(Color.red);
        //bar.setSize(100, 1000);
        bar.setStringPainted(true);
        
        frame.add(bar);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setLayout(null);
        frame.setSize(450,450);
        frame.setVisible(true);
        
        fill();
    }

    private void fill() {
        int counter = 0;
        //bar.setValue(counter);
        
        while(counter<=100){
            bar.setValue(counter);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ProgressBarFeatures.class.getName()).log(Level.SEVERE, null, ex);
            }
            counter+=10;
        }
        bar.setString("Done!");
        
    }
}
