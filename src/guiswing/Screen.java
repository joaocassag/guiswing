/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author joaocassamano
 */
public class Screen extends JFrame implements ActionListener {
    
    JButton button1;
    Screen(){
        
        //label
        JLabel label1 = new JLabel();
        label1.setText("Sample label");
        
        ImageIcon icon = new ImageIcon("src/resources/save.png");
        Border border = BorderFactory.createLineBorder(Color.yellow,3);
        
        this.setIconImage(icon.getImage());
        label1.setIcon(icon);
        label1.setHorizontalTextPosition(JLabel.LEFT);
        label1.setVerticalTextPosition(JLabel.TOP);
        label1.setForeground(Color.red);
        label1.setFont(new Font("Time New Roman", Font.PLAIN,12));
        label1.setIconTextGap(-25);
        label1.setBackground(Color.blue);
        label1.setOpaque(true);
        label1.setBorder(border);
        label1.setVerticalAlignment(JLabel.CENTER);
        label1.setHorizontalAlignment(JLabel.CENTER);
        label1.setBounds(200,200,125,125);
        
        
        //panel
        JPanel panel1 = new JPanel();
        panel1.setBackground(Color.red);
        panel1.setBounds(0, 0, 150, 150);
        panel1.setLayout(new BorderLayout());
        
        //JButton
        button1 = new JButton();
        button1.setBounds(200,200,100,50);
        button1.addActionListener(this);
        //button1.addActionListener(e ->{ System.out.println("Clicked button1");} );
        button1.setText("Button 1");
        button1.setFocusable(false);
        //button1.setIcon(icon);
        //button1.setHorizontalTextPosition(JButton.CENTER);
        //button1.setVerticalTextPosition(JButton.TOP);
        //button1.setIconTextGap(-10);
        //button1.setForeground(Color.white);
        //button1.setBackground(Color.DARK_GRAY);
        //button1.setBorder(BorderFactory.createEtchedBorder());
        //button1.setEnabled(false);
        
        
        
        
        //frame
        this.setTitle("JFrame sample");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //this.setResizable(false);
        this.setSize(600,600);
        this.setLayout(null);
        ImageIcon frame_icon = new ImageIcon("src/resources/dot.png");
        this.setIconImage(frame_icon.getImage());
        this.getContentPane().setBackground(new Color(115,125,200));
        
        //add objects to panel1
        panel1.add(label1);
        
        
        //add objets to frame
        //this.add(label1);
        this.add(panel1);
        this.add(button1);
        
        //adjust to frame contents
        //this.pack();
        //make frame visible
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button1){
            System.out.println("Clicked button1");
        }
    }
    
}
