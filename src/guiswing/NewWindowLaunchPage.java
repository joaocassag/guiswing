/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author joaocassamano
 */
public class NewWindowLaunchPage implements ActionListener {
    
    JFrame frame = new JFrame();
    JButton button = new JButton("New Window");
    
    NewWindowLaunchPage(){
        
        button.setBounds(100,160,200,40);
        button.setFocusable(false);
        button.addActionListener(this);
        
        frame.add(button);
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(420,420);
        frame.setLayout(null);
        
        
        frame.setVisible(true);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame.dispose();
        if(e.getSource()==button){
            NewWindow nw = new NewWindow();
        }
    }
}
