/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author joaocassamano
 */
public class TextFieldFrame extends JFrame implements ActionListener {
    JButton button;
    JTextField textfield;
    
    TextFieldFrame(){
        
        //frame.setTitle("JFrame sample");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        
        button = new JButton("Submit");
        button.addActionListener(this);
        
        
        textfield = new JTextField();
        textfield.setPreferredSize(new Dimension(250,40));
        textfield.setFont(new Font("Times", Font.PLAIN,35));
        textfield.setForeground(Color.blue);
        textfield.setBackground(Color.red);
        textfield.setCaretColor(Color.green);
        //textField.
        
        
        this.add(button);
        this.add(textfield);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button){
            System.out.println("Welcome "+textfield.getText());
        }
    }
    
}
