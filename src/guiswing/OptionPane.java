/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiswing;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author joaocassamano
 */
public class OptionPane {
    public static void main(String[] args){
        /*JFrame frame = new JFrame();
        frame.setTitle("JFrame sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setVisible(true);*/
        
        //JOptionPane.showMessageDialog(null, "The message goes here", "title", JOptionPane.PLAIN_MESSAGE);
        //JOptionPane.showMessageDialog(null, "The message goes here", "title", JOptionPane.INFORMATION_MESSAGE);
        //JOptionPane.showMessageDialog(null, "The message goes here", "title", JOptionPane.QUESTION_MESSAGE);
        //JOptionPane.showMessageDialog(null, "The message goes here", "title", JOptionPane.WARNING_MESSAGE);
        //JOptionPane.showMessageDialog(null, "The message goes here", "title", JOptionPane.ERROR_MESSAGE);
        
        //int answer = JOptionPane.showConfirmDialog(null, "message", "", JOptionPane.YES_NO_CANCEL_OPTION);
        //String input = JOptionPane.showInputDialog("Some input here");
        String options[] ={"A","B","C"};
        JOptionPane.showOptionDialog(null, "message", "title", JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE,null, options, 0);
    }
    
}
