
package guiswing;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author joaocassamano
 */
public class KeyListenerFrame extends JFrame implements KeyListener {
    JLabel label;
    
    KeyListenerFrame(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500,500);
        this.setLayout(null);
        
        this.addKeyListener(this);
        
        label = new JLabel("Label");
        label.setBounds(0,0,100,100);
        label.setBackground(Color.red);
        label.setOpaque(true);
        
        //this.getContentPane().setBackground(Color.blue);
        
        
        this.add(label);
        this.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println(new Date()+" key Typed Out is  "+e.getKeyChar());
        switch(e.getKeyChar()){
            case 'a': 
                System.out.println(new Date()+" key Typed In is  "+e.getKeyChar());
                label.setLocation(label.getX()-10, label.getY());
                break;
            case 'w': 
                label.setLocation(label.getX(), label.getY()-10);
                break;
            case 's': 
                label.setLocation(label.getX(), label.getY()+10);
                break;
            case 'd': 
                label.setLocation(label.getX()+10, label.getY());
                break;
            
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case 37: 
                System.out.println(new Date()+" key Pressed In is  "+e.getKeyChar());
                label.setLocation(label.getX()-10, label.getY());
                break;
            case 38: 
                label.setLocation(label.getX(), label.getY()-10);
                break;
            case 39: 
                label.setLocation(label.getX()+10, label.getY());
                break;
            case 40: 
                label.setLocation(label.getX(), label.getY()+10);
                break;
            
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //System.out.println(" key Released is "+e.getKeyChar());
        //System.out.println(" key Released code "+e.getKeyCode());
    }
    
}
