
package guiswing;

import javax.swing.JFrame;

/**
 *
 * @author joaocassamano
 */
public class DragFrame extends JFrame {
    
    DragPanel dragPanel = new DragPanel();
    
    DragFrame(){
        this.add(dragPanel);
        
        this.setTitle("Drag");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        this.setVisible(true);
    }
}
